var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/partnerAPI', function (req, res) {
  console.log(`--------BODY----------\n%o\n--------END BODY------`, req.body);
  res.status(200).send('Ok');
});

module.exports = router;
